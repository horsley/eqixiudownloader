// eqixiuDownloader project main.go
package main

import (
	"bufio"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"time"
)

const (
	JSON_API    = "http://s2.eqxiu.com/eqs/page/"
	RES_API_PFX = "http://res.eqh5.com/"
)

func main() {
	if len(os.Args) < 2 {
		errorLog("请把地址列表文件拖动到本程序上")
		return
	}
	inputFile := os.Args[1]

	f, err := os.Open(inputFile)
	defer f.Close()
	if err != nil {
		errorLog("打开列表文件：" + inputFile + "失败：" + err.Error())
		return
	}

	s := bufio.NewScanner(f)

	absPath, _ := filepath.Abs(inputFile)
	rootDir := filepath.Dir(absPath)
	for s.Scan() {
		url := s.Text()

		subDir := filepath.Base(url)
		err := os.Mkdir(subDir, 0755)

		if err != nil {
			errorLog("创建子目录" + subDir + "失败：" + err.Error())
		}

		imgs := getEqixiuImages(url)

		if imgs == nil {
			errorLog("网址：" + url + "获取图片失败")
			continue
		}

		for _, imgUrl := range imgs {
			err := downloadFileToDir(imgUrl, filepath.Join(rootDir, subDir))
			if err != nil {
				errorLog("下载文件" + imgUrl + "失败：" + err.Error())
			}
		}
	}

}

func downloadFileToDir(url, dir string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	bin, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	filename := filepath.Join(dir, filepath.Base(url))
	return ioutil.WriteFile(filename, bin, 0644)
}

func getEqixiuImages(url string) []string {
	landing := httpGet(url)
	sid := getSceneId(landing)

	pageData := httpGet(JSON_API + sid)
	//	fmt.Println(pageData)

	imgRe := regexp.MustCompile(`"imgSrc":"(.*?)"`)
	m := imgRe.FindAllStringSubmatch(pageData, -1)

	rst := make([]string, 0)
	for _, v := range m {
		rst = append(rst, RES_API_PFX+v[1])
	}

	return rst
}

func getSceneId(html string) string {
	idRe := regexp.MustCompile(`scene = {id:(\d+),`)

	m := idRe.FindStringSubmatch(html)
	return m[1]
}

func httpGet(url string) string {
	resp, err := http.Get(url)
	if err != nil {
		return ""
	}
	defer resp.Body.Close()

	bin, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ""
	}

	return string(bin)
}

func errorLog(msg string) {
	t := time.Now().Format("2006-01-02 15:04:05 ")
	ioutil.WriteFile(filepath.Join(filepath.Dir(os.Args[0]), "log.txt"), []byte(t+msg), 0666)
}
